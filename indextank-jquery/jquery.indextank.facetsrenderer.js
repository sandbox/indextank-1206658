(function($){
   if(!$.Indextank){
        $.Indextank = new Object();
    };
    
    $.Indextank.FacetsRenderer = function(el, options){
        // To avoid scope issues, use 'base' instead of 'this'
        // to reference this class from internal events and functions.
        var base = this;
        
        // Access to jQuery and DOM versions of element
        base.$el = $(el);
        base.el = el;
        
        // Add a reverse reference to the DOM object
        base.$el.data("Indextank.FacetsRenderer", base);
        
        base.init = function(){
            base.options = $.extend({},$.Indextank.FacetsRenderer.defaultOptions, options);


            base.$el.bind( "Indextank.AjaxSearch.success", function (event, data) {
                base.$el.show();
                base.$el.html("");

                var queriedFacets = data.query.categoryFilters || {};
                
                var $selectedFacetsContainer = $("<ul/>").attr("id", "indextank-selected-facets");
                var $availableFacetsContainer = $("<div/>").attr("id", "indextank-available-facets");
                
                $.each( data.facets, function (catName, values){
                    if (catName in queriedFacets) {
                        var $selectedFacet = base.renderSelectedFacet(queriedFacets, catName, data);
                        $selectedFacetsContainer.append($selectedFacet);
                    } else {
                        var $availableFacet = base.renderAvailableFacet(queriedFacets, catName, values, data);
                        $availableFacetsContainer.append($availableFacet);
                    }
                });

                base.$el.append($selectedFacetsContainer, $availableFacetsContainer);
            });
        };
        
        base.renderSelectedFacet = function(queriedFacets, categoryName, data) {
            // Render selected facet as a <li/> and return it
            $item = $("<li/>");
            
            $selectedCategory = $("<span/>").addClass("indextank-selected-facet-container").append(
                $("<a/>").attr("href","#")
                    .append($("<span/>").text(categoryName + " - " + queriedFacets[categoryName]))
                    .append($("<span/>").addClass("ui-icon ui-icon-close").text("[Remove]"))
                    .click(function(){
                        // ensure query data has something on it
                        var query = data.query.clone();
                        query.withoutCategories([categoryName]);
                        // start over!
                        query.withStart(0);
                        data.searcher.trigger("Indextank.AjaxSearch.runQuery", [query]);
                    })
            );

            $item.append($selectedCategory);

            return $item;
        }


        base.renderAvailableFacet = function(queriedFacets, categoryName, categoryValues, data) {
            // Render available facet as a <dl> (definition list) and return it

            $facetContainer = $("<div/>").addClass("indextank-available-facet-container");
            $availableFacet = $("<dl/>");
            $facetContainer.append($availableFacet);
            $availableFacet.append($("<dt/>").text(categoryName));

            // find out if we should collapse facets, or not
            sorted = []
            $extraValues = $();
            $.each(categoryValues, function( ignored, count) { sorted.push(count);});

            if (sorted.length > 4 ) {
                $more = $("<div/>").addClass("more");
                $btn = $("<span/>").text("more " + categoryName + " options").button().data("mymore",$more);

                $extraValues = $("<dl/>");
                $more.append($extraValues);

                $btn.click(function(event) {
                    // need to call parents here, as 'button' messes up objects.
                    $(event.target).parents().data("mymore").dialog("open");
                } );

                $facetContainer.append($btn);
                $facetContainer.append($more);
                sorted = sorted.sort( function(a,b) { return b-a;});
                
                $more.dialog({autoOpen:false});
            }

            threshold = sorted.length > 4 ? sorted[4] : 0;

            // for this category, render all the controls 
            $.each(categoryValues, function (categoryValue, count) {
                var dd = $("<dd/>").append(
                    $("<a/>")
                    .attr("href", "#")
                    .text(categoryValue + " (" + count + ")")
                    .click(function(){
                        // ensure query data has something on it
                        var query = data.query.clone();
                        filter = {};
                        filter[categoryName] = categoryValue;
                        query.withCategoryFilters(filter);
                        // start over!
                        query.withStart(0);
                        data.searcher.trigger("Indextank.AjaxSearch.runQuery", [query]);
                    })
                );

                dd.data("Indextank.FacetsRenderer.catValue", categoryValue);
                dd.data("Indextank.FacetsRenderer.catName", categoryName);
                dd.data("Indextank.FacetsRenderer.searcher", data.searcher);

                if (count >= threshold ) { 
                    $availableFacet.append(dd);
                } else { 
                    $extraValues.append(dd);
                }
            });
            return $facetContainer;
        }
        
        // Run initializer
        base.init();
    };
    
    $.Indextank.FacetsRenderer.defaultOptions = {
    };
    
    $.fn.indextank_FacetsRenderer = function(options){
        return this.each(function(){
            (new $.Indextank.FacetsRenderer(this, options));
        });
    };
    
})(jQuery);
