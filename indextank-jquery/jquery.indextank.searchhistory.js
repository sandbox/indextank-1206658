(function($){
    if(!$.Indextank){
        $.Indextank = new Object();
    };
    
    $.Indextank.SearchHistory = function(el, options){
        // To avoid scope issues, use 'base' instead of 'this'
        // to reference this class from internal events and functions.
        var base = this;
        
        // Access to jQuery and DOM versions of element
        base.$el = $(el);
        base.el = el;
        
        // Add a reverse reference to the DOM object
        base.$el.data("Indextank.SearchHistory", base);
        
        base.init = function(){
            base.options = $.extend({},$.Indextank.SearchHistory.defaultOptions, options);
            
            // Put your initialization code here
            base.$el.bind("Indextank.AjaxSearch.success", function(e) {
                base.$el.children(base.options.elemsToHide).each( function(index, elem){
                    $(elem).hide();
                });
                $("#indextank-renderer").show();
            });
        };
        
        // Run initializer
        base.init();
    };

    $.Indextank.SearchHistory.defaultOptions = {
        // a selector to hide when searching
        elemsToHide: "*"
    };

    $.fn.indextank_SearchHistory = function(options){
        return this.each(function(){
            (new $.Indextank.SearchHistory(this, options));
        });
    };
    
})(jQuery);
