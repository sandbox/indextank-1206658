/*
 * Convenient format function to display results' dates.
 */
Date.prototype.format = function() { 
    var shortDayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var formattedDate = "";
    formattedDate += shortDayNames[this.getDay()] + ", ";
    formattedDate += (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear() + " - ";
    formattedDate += this.getHours() + ":" + this.getMinutes();
    return formattedDate;
};



// Don't forget to declare var INDEXTANK_PUBLIC_URL, INDEXTANK_INDEX_NAME and INDEXTANK_CONTAINER_SELECTOR
(function($){
  $(document).ready(function() { 
    
    // IndexTank-ize the search form
    $("#indextank-form").indextank_Ize( INDEXTANK_PUBLIC_URL, INDEXTANK_INDEX_NAME);


    // Options for IndexTank-InstantLinks
    var opt = {
      name: "title",
      fields: "title,url,content",
      format: function( item , options ) {
        function hl(text, query){
          text = decodeURIComponent( escape( text ) );
          rx = new RegExp(query,'ig');
          bolds = $.map(text.match(rx) || [], function(i) { return "<span class='highlighted'>"+i+"</span>";});
          regulars = $( $.map(text.split(rx), function(i){ return $("<span></span>").addClass("regular").text(i).get(0);}));

          return $.each(regulars, function(index, value) {
              $(value).append(bolds[index] || "");
          });
        };

        var name = item[options.name];
        var highlightedName = hl(name, item.queryTerm);

        var l =  $("<a></a>").attr("href", item[options.url]);

        // only display images for those documents that have one
        if (item[options.thumbnail]) {
          l.addClass("with-thumbnail");
          l.append( $("<img />")
            .attr("src", item[options.thumbnail])
            .css( { "max-width": "50px", "max-height": "50px"} ) ) ;
        }

        l.append( $("<span/>").addClass("name").append(highlightedName) );

        // only add description for those documents that have one
        if (item[options.description]) { 
          l.addClass("with-description");
          l.append( $("<span/>").addClass("description").text(item[options.description]));
        }

        return l;
      }
    };

    $("#indextank-query").indextank_InstantLinks(opt);

    var fmt = function(item){
      var d = new Date(item.timestamp * 1000);

      return $("<div/>").addClass("node")
              .append( $("<h2/>")
              .append( $("<a/>").attr("href", item.url).text(decodeURIComponent( escape( item.title ) ) ) ) )
              .append( $("<div/>").addClass("meta").addClass("submitted")
              .text("submitted by " + item.user + " on " + d.format()))
              .append( $("<div/>").addClass("content").addClass("clearfix").html(decodeURIComponent( escape( item.snippet_content ) ) ))
              .append( $("<div/>").addClass("link-wrapper"));
    };

    // Listeners

    var container = $(INDEXTANK_CONTAINER_SELECTOR).attr("id", "indextank-container");

    var indextankRenderer = $("<div/>").attr("id", "indextank-renderer").css("display", "block");
    var facets = $("<div/>").attr("id", "indextank-facets");
    var results = $("<div/>").attr("id", "indextank-results");
    var pagination = $("<div/>").attr("id", "indextank-pagination");
    indextankRenderer.append(facets, results, pagination).hide();

    facets.indextank_FacetsRenderer();
    results.indextank_Renderer({format: fmt});
    pagination.indextank_Pagination();

    container.prepend(indextankRenderer);

    container.indextank_SearchHistory({elemsToHide: 'div[id!="footer"]'});

    // Stats renderer
    $("#indextank-form").append($("<div></div>").attr("id", "indextank-stats"));
    $("#indextank-stats").indextank_StatsRenderer();

    var listeners = $("#indextank-stats,#indextank-facets,#indextank-results,#indextank-pagination,#indextank-container");

    var rw = function(q) {
      return "title:(" + q + "*)^5 OR content:(" + q + "*)";
    };

    $("#indextank-query").indextank_AjaxSearch({
      listeners: listeners, 
      rewriteQuery: rw, 
      fields: "title,content,user,timestamp,url", 
      snippets: "content"
    });

  }); 
})(jQuery);
