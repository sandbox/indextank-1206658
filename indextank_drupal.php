<?php

require_once(dirname(__FILE__) . '/indextank.php');

/**
 * A convinient function to get an index object.
 */
function indextank_get_index() {
  $api_url = variable_get('indextank_apiurl', '');
  if ($api_url != '') {
    // Index name used to be 'drupal'. With autoprovisioning feature
    // 'idx' is the default name. 
    $index_name = variable_get('indextank_index_name', 'drupal');
  
    $api = new Indextank_Api($api_url);
    $index = $api->get_index($index_name);
    return $index;
  } else {
    return NULL;
  }
}

/**
 * Select all nodes from DB and index them in IndexTank
 */
function indextank_index_all_nodes($index) {
  if ($index != NULL) {
    // Select all your nodes with db_query...
    if (indextank_is_drupal_version_6()) {
      $rows = db_query(
      "SELECT n.nid, n.type, n.title, n.created as timestamp, r.body as body, u.name name
         FROM {node} n, {node_revisions} r, {users} u
        WHERE n.nid = r.nid
          AND n.uid = u.uid
          AND n.status = 1;");
      $results = array();
      while ($row = db_fetch_object($rows)) {
        $results[] = $row;
      } 
    } 
    else {
      $results = db_query(
      "SELECT n.nid, n.type, n.title, n.created as timestamp, fd.body_value as body, u.name name
         FROM node n, field_data_body fd, users u
        WHERE n.nid = fd.entity_id
          AND n.uid = u.uid
          AND n.status = 1;", array()
      );
    }

    // and add all your nodes to IndexTank
    $docs = array();
    foreach ($results as $row) {
      $fields = indextank_get_fields($row);
      $categories = indextank_get_categories($row);
      $doc = array('docid' => $row->nid, 'fields' => $fields, 'categories' => $categories);
      $docs[] = $doc;

      if (count($docs) % 100 == 0) {
        $index->add_documents($docs);
        $docs = array();
      }
    }
    
    if (count($docs) > 0) {
        $out = $index->add_documents($docs);
    }
  }
}

/**
 * Sends a document to IndexTank
 */
function indextank_index_document($index, $document) {
  $data = indextank_get_fields($document);
  $categories = indextank_get_categories($document);
  $docid = $document->nid;
  $index->add_document($docid, $data, NULL, $categories);
}

/**
 * Removes a document from IndexTank
 */
function indextank_delete_document($index, $document) {
  $docid = $document->nid;
  $index->delete_document($docid);
}

/**
 * Transform a Drupal's document into a associative array.
 *
 * The associative array contains the following fields:
 * - url: the document url
 * - title: the document title
 * - content: the document content
 * - user: the document owner
 * - timestamp: the creation date of the document
 */
function indextank_get_fields($document) {
  if (is_array($document->body)) {
    $text = utf8_encode($document->body['und'][0]['value']);
  }
  else {
    $text = utf8_encode($document->body);
  }
  $fields = array(
    'url' => url("node/{$document->nid}"),
    'title' => utf8_encode($document->title),
    'content' => $text,
    'user' => $document->name,
    'timestamp' => $document->timestamp,
  );

  return $fields;
}

/**
 * Extract an associative array with categories from a Drupal's document.
 *
 * The associative array contains the following fields:
 * - Date: 'Month Year'
 * - Content Type: [Page|Story]
 */
function indextank_get_categories($document) {
  $categories = array(
    'Date' => date('F Y', $document->timestamp),
    'Content Type' => $document->type,
  );

  return $categories;
}
/**
 * Validates that a string has the form of an IndexTank's api url.
 * 
 * @param $api_url
 *   Should match this regexp:
 *   /^http:\/\/:[[:alnum:][:punct:]]+@[a-z0-9]{3,8}\.api\.indextank\.com$/
 *
 * @return
 *   TRUE if and only if $api_url matches the regexp
 */
function indextank_valid_apiurl($api_url) {
  return (bool) preg_match("
      /^http:\/\/:[[:alnum:][:punct:]]+@[a-z0-9]{3,8}\.api\.indextank\.com$/",
      $api_url);
}

/**
 * A convinient method to check if drupal version is 6.x
 */
function indextank_is_drupal_version_6() {
  $ret = defined("VERSION") &&  substr(VERSION,0,1) == '6';
  return $ret;
}
