
-- SUMMARY --
IndexTank module for Drupal
http://www.indextank.com

This module integrates search with Drupal and replaces the core content search
with hosted search solutions provided by IndexTank. IndexTank module allows for
faster search results and provides users with Full-Text Search, Instant-search
& Facets. Also, if any help is needed, follow on to http://www.indextank.com
and chat with the developer who actually contributed to the module.

IndexTank module is compatible with Drupal 6 & Drupal 7. After installing the
module, IndexTank indexes all the documents and hosts in the cloud for faster
search results. First 100,000 documents (nodes) are free, and if a larger
account is needed, please visit http://www.indextank.com to find solutions that
fit your requirements.

-- REQUIREMENTS --
* Php 5
* php5-curl module
* Drupal 6.x or 7.x
* Drupal Search module

-- INSTALLATION --

1.- Download and unzip/untar the last stable version of the IndexTank module
    for Drupal.
2.- Upload the 'indextank' module to <your_drupal_directory>/sites/all/module 
    in your web server.
3.- Enable the IndexTank and Search module at Administer >> Site Building >>
    Modules.
4.- Easy, isn't it?
